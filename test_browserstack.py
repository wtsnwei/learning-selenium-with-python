import sys
import unittest
from selenium import webdriver


class SearchProducts(unittest.TestCase):
    BROWSERSTACK_URL = 'https://ehnpqz1:9vozkqkGTuQpjypPS2QW@hub-cloud.browserstack.com/wd/hub'

    def setUp(self):
        desired_cap = {
            'os': 'Windows',
            'os_version': '10',
            'browser': 'Chrome',
            'browser_version': '80',
            'name': "ehnpqz1's First Test"
        }
        self.driver = webdriver.Remote(command_executor=self.BROWSERSTACK_URL, desired_capabilities=desired_cap)
        self.driver.get("http://demo-store.seleniumacademy.com/")
        self.driver.implicitly_wait(30)

    def tearDown(self):
        self.driver.quit()

    def test_search_by_category(self):
        # get the search textbox
        self.search_field = self.driver.find_element_by_name("q")
        self.search_field.clear()

        # enter search keyword and submit
        self.search_field.send_keys('books')
        self.search_field.submit()

        # get all the anchor elements which have product names
        # displayed currently on result page using # find_elements_by_xpath method
        products = self.driver.find_elements_by_xpath("//h2[@class='product-name']/a")

        # check count of products shown in results
        self.assertEqual(3, len(products))


if __name__ == "__main__":
    if len(sys.argv) > 1:
        SearchProducts.BROWSER = sys.argv.pop()
        SearchProducts.PLATFORM = sys.argv.pop()
    unittest.main(verbosity=2)
