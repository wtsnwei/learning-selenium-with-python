import time
import unittest

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

from base import CustomizeBaseTest


class HomePageTest(CustomizeBaseTest):

    def test_language_options(self):
        # list of expected values in Language dropdown
        exp_options = ['English', 'French', 'German']

        # empty list for capturing actual options displayed in the dropdown
        act_options = []

        # get the Your language dropdown as instance of Select class
        select_language = Select(self.driver.find_element_by_id("select-language"))

        # check number of options in dropdown
        self.assertEqual(3, len(select_language.options))

        # get options in a list
        for option in select_language.options:
            act_options.append(option.text)

        # check expected options list with actual options list
        self.assertEqual(exp_options, act_options)

        # check default selected option is English
        self.assertEqual("English", select_language.first_selected_option.text)

        # select an option using select_by_visible text
        select_language.select_by_visible_text("German")
        # check store is now German
        self.assertTrue("store=german" in self.driver.current_url)

        # changing language will refresh the page,
        # we need to get find language dropdown once again
        select_language = Select(self.driver.find_element_by_id("select-language"))
        select_language.select_by_index(0)

    def test_search_text_field_max_length(self):
        search_field = self.driver.find_element_by_id("search")
        self.assertEqual("128", search_field.get_attribute("maxlength"))

    def test_search_button_enabled(self):
        search_button = self.driver.find_element_by_class_name("button")
        self.assertTrue(search_button.is_enabled())

    def test_my_account_link_is_displayed(self):
        account_link = self.driver.find_element_by_link_text("ACCOUNT")
        self.assertTrue(account_link.is_displayed())

    def test_account_links(self):
        account_links = self.driver.find_elements_by_partial_link_text("ACCOUNT")
        self.assertTrue(2, len(account_links))

    def test_count_of_promo_banners_images(self):
        banner_list = self.driver.find_element_by_class_name("promos")
        banners = banner_list.find_elements_by_tag_name("img")
        self.assertEqual(3, len(banners))

    def test_vip_promo(self):
        vip_promo = self.driver.find_element_by_xpath("//img[@alt='Shop Private Sales - Members Only']")
        self.assertTrue(vip_promo.is_displayed())

        # click on vip promo images to open the page
        vip_promo.click()
        time.sleep(10)
        self.assertEqual('VIP', self.driver.title)

    def test_shopping_cart_status(self):
        shopping_cart_icon = self.driver.find_element_by_css_selector("div.header-minicart span.icon")
        shopping_cart_icon.click()

        shopping_cart_status = self.driver.find_element_by_css_selector("p.empty").text
        self.assertEqual("You have no items in your shopping cart.", shopping_cart_status)

        close_button = self.driver.find_element_by_css_selector("div.minicart-wrapper a.close")
        close_button.click()

    def test_search_field(self):
        self.assertTrue(self.is_element_present(By.NAME, "q"))

    def test_language_option(self):
        # check language options dropdown on Home page
        self.assertTrue(self.is_element_present(By.ID, "select-language"))

    def test_shopping_cart_empty_message(self):
        # check content of My Shopping Cart block on Home page
        shopping_cart_icon = self.driver.find_element_by_css_selector("div.header-minicart span.icon")
        shopping_cart_icon.click()

        shopping_cart_status = self.driver.find_element_by_css_selector('p.empty').text
        self.assertEqual("You have no items in your shopping cart.", shopping_cart_status)

        close_button = self.driver.find_element_by_css_selector("div.minicart-wrapper a.close")
        close_button.click()

    def is_element_present(self, how, what):
        """
        Utility method to check presence of an element on page
        :params how: By locator type
        :params what: locator value
        """
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False

        return True


if __name__ == '__main__':
    unittest.main(verbosity=2)
