import sys
import unittest
from selenium import webdriver


class SearchProducts(unittest.TestCase):
    PLATFORM = 'WINDOWS'
    BROWSER = 'firefox'

    def setUp(self):
        firefox_options = webdriver.FirefoxOptions()
        firefox_options.set_capability("platform", self.PLATFORM)
        firefox_options.set_capability("browserName", self.BROWSER)
        self.driver = webdriver.Remote(
            command_executor='http://127.0.0.1:4444/wd/hub',
            options=firefox_options
        )
        self.driver.get("http://demo-store.seleniumacademy.com/")
        self.driver.implicitly_wait(30)

    def tearDown(self):
        self.driver.quit()

    def test_search_by_category(self):
        # get the search textbox
        self.search_field = self.driver.find_element_by_name("q")
        self.search_field.clear()

        # enter search keyword and submit
        self.search_field.send_keys('books')
        self.search_field.submit()

        # get all the anchor elements which have product names
        # displayed currently on result page using # find_elements_by_xpath method
        products = self.driver.find_elements_by_xpath("//h2[@class='product-name']/a")

        # check count of products shown in results
        self.assertEqual(3, len(products))


if __name__ == "__main__":
    if len(sys.argv) > 1:
        SearchProducts.BROWSER = sys.argv.pop()
        SearchProducts.PLATFORM = sys.argv.pop()
    unittest.main()
