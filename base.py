import unittest
from selenium import webdriver
from selenium.webdriver.common.proxy import Proxy, ProxyType

PROXY = Proxy({
    'proxyType': ProxyType.MANUAL,
    'httpProxy': '127.0.0.1:7890',
    'noProxy': ''
})


class CustomizeBaseTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Firefox()
        # 隐式等待60秒
        cls.driver.implicitly_wait(60)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def setUp(self):
        self.driver.get("http://demo-store.seleniumacademy.com/")
