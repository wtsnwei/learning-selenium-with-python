import unittest
import HtmlTestRunner

from test_compare_products import CompareProductsTest
from test_navigation import NavigationTest
from test_register import RegisterTest
from test_search_products import SearchTests
from test_home_page import HomePageTest


class SmokeTest(object):

    @staticmethod
    def run():
        # get all tests
        search_test = unittest.TestLoader().loadTestsFromTestCase(SearchTests)
        home_page_tests = unittest.TestLoader().loadTestsFromTestCase(HomePageTest)
        register_test = unittest.TestLoader().loadTestsFromTestCase(RegisterTest)
        navigation_test = unittest.TestLoader().loadTestsFromTestCase(NavigationTest)
        compare_test = unittest.TestLoader().loadTestsFromTestCase(CompareProductsTest)

        # create a test suite
        smoke_tests = unittest.TestSuite([
            home_page_tests,
            search_test,
            register_test,
            navigation_test,
            compare_test
        ])

        # configure HTMLTestRunner options
        runner = HtmlTestRunner.HTMLTestRunner(output='smoke_tests')

        # run the suite using HTMLTestRunner
        runner.run(smoke_tests)


if __name__ == "__main__":
    smoke_test = SmokeTest()
    smoke_test.run()
